<?php


class Country
{
    /**
     * Country constructor.
     */
    private $name;
    private $continent;
    private int $population;

    /**
     * Country constructor.
     * @param $name
     * @param $continent
     * @param int $population
     */
    public function __construct($name, $continent, int $population)
    {
        $this->name = $name;
        $this->continent = $continent;
        $this->population = $population;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getContinent()
    {
        return $this->continent;
    }

    /**
     * @param mixed $continent
     */
    public function setContinent($continent): void
    {
        $this->continent = $continent;
    }

    /**
     * @return int
     */
    public function getPopulation(): int
    {
        return $this->population;
    }

    /**
     * @param int $population
     */
    public function setPopulation(int $population): void
    {
        $this->population = $population;
    }





}