<?php

    include 'Countries.php';

class CountriesTest extends \PHPUnit\Framework\TestCase
{
    private $countries;

    protected function SetUp() :void{
        $this->countries = new Countries();
    }

    public function testPopulation(){
        $countries = $this->countries->getCountryByPopulation(144);

        $this->assertEquals(1, $this->count($countries));
    }

    /**
     * @dataProvider addDataProvider
     * @param $continent
     * @param $expected
     */
    public function testContinentCount($continent, $excepted){

        $result =$this->countries->getCountriesCountByContinent($continent);
        $this->assertEquals($excepted, $result);
    }

    public function addDataProvider() {
        return array(
            array('Eurasia', 3),
            array('Eurasia', 2),
            array('Eurasia', 0),
        );
    }
    protected function tearDown() : void
    {
        isset($this->countries);
    }


}