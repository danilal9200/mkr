<?php

    include 'Country.php';

class Countries
{
    private $countries;

    /**
     * Countries constructor.
     * @param $countries
     */
    public function __construct()
    {
        $this->countries = array(
            new Country('Ukraine', 'Eurasia', 44),
            new Country('Russia', 'Eurasia', 144 ),
            new Country('USA', 'North America', 329),
        );
    }

    public function getCountryByPopulation($population){
        foreach ($this->countries as $country){
            if ($country->getPopulation() == $population )
                return $country->getName();
        }

        return null;

    }

    public function getCountriesCountByContinent($continent){
        $count = 0;

        foreach ($this->countries as $country){
            if($country->getContinent() == $continent){
                $count++;
            }
        }
        return $count;
    }




}